# Hexo Markdown to Gemtext

This python script takes Markdown blog post from [Hexo](https://hexo.io) blog and convert them to Gemini gemtext file, it also convert `about` page is there is one, and create `index.gmi` files with the template in the script.

For more informations about the Gemini protocol, check the [official website](https://gemini.circumlunar.space/)

## Installation

```
git clone https://framagit.org/blchrd/hexo-markdown-to-gemtext
cd hexo-markdown-to-gemtext
pip install -r requirements.txt
```

## Usage

Change the values in the script if needed, you can also add any static assets directories if you want to (the directories will be copied in full in the `root_gmi` folder).

Update `index.template` with your template.

```
python main.py [hexo-source-dir]
```

*All converted files will be outputed in `public_gmi` by default, you can change this dir in the script.*

**Caution, importing tags and categories are not supported**