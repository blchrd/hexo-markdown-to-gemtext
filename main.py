import os
import shutil
from pathlib import Path
from md2gemini import md2gemini

# ===== UPDATE THESE VALUES ===== #
host = "gemini://blchrd.eu/"
author = "blchrd"
with open('index.template', 'r') as template: 
    index_template = template.read()
# =============================== #

root_gmi = "public_gmi"
source_dir = sys.argv[1]
assets_directories = []
blog_posts = []
pages = []


def copy_assets_dir(dir):
    print(f"Copy assets directory {source_dir}/{dir} to {root_gmi}")
    shutil.copytree(f"{source_dir}/{dir}", f"{root_gmi}/{dir}", symlinks=False, ignore=None, ignore_dangling_symlinks=False, dirs_exist_ok=True)


def create_index_page(blog_post_list, page_list):
    print(f"Create index links list in {root_gmi}/index.gmi")
    blog_post_links = ""
    page_links = ""
    for blog_post in blog_post_list:
        blog_post_links += f"=> {blog_post}"
    for page in page_list:
        page_links += f"=> {page}"
    gemini_index = index_template.format(blog_post_links, page_links)
    
    with open(f"{root_gmi}/index.gmi",'w') as wf:
        wf.write(gemini_index)
        

def transform_markdown_into_gemtext(source, file_dest, blog_post=True):
    with open(source, "r") as f:
        md_content = f.read().splitlines(True)
        title = ""
        date = ""
        header_start = False
        index_start = 0
        
        for line in md_content:
            index_start += 1
            if line.startswith('---'):
                if header_start:
                    break
                header_start = True
            elif line.startswith('title:'):
                title = line.replace('title:', '').lstrip()
            elif line.startswith('date:'):
                date = line.replace('date:', '').strip()
            elif line.startswith('tags'):
                pass
    
        md_content = md_content[index_start:]
        formated_date = date[:10]
        if blog_post:
            md_content.insert(0, f"Author: {author} - Date: {formated_date}\r\n")
            md_content.insert(0, f"# {title}")
            file_dest = f'{formated_date}-{file_dest}'
        
        dest = f"{root_gmi}/{file_dest}"
        print(f"Transform '{source}' into '{dest}'")
        
        md_content = ''.join(md_content)
        gemini = md2gemini(md_content, links="at-end", plain=True)
        with open(dest, 'w') as wf:
            wf.write(gemini)
            if blog_post:
                blog_posts.append(f'{host}{file_dest}  {formated_date}: {title}')
            else:
                pages.append(f'{host}{file_dest}   {title}')
            

Path(root_gmi).mkdir(parents=True, exist_ok=True)
if os.path.isfile(f'{source_dir}/about/index.md'):
    transform_markdown_into_gemtext(f'{source_dir}/about/index.md', 'about.gmi', blog_post=False)
for root, dirs, files in os.walk(f'{source_dir}/_posts'):
    for file in files:
        transform_markdown_into_gemtext(f'{root}/{file}', file.replace('.md', '.gmi'))

for assets_dir in assets_directories:
    copy_assets_dir(assets_dir)

blog_posts.sort(reverse=True)
create_index_page(blog_posts, pages)